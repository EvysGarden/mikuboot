# Mikuboot
A miku boot animation because she's living in my computer.

Inspired by [ooo.eeeee.ooo](https://ooo.eeeee.ooo) and the song Miku by Anamanaguchi.

![mikuboot boot animation](mikuboot/source.gif)
