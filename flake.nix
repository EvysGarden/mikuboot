{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, utils, nixpkgs, ... }:
    utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; };
      in {
        packages.default = pkgs.stdenv.mkDerivation
          {
            name = "mikuboot-plymouth-theme";
            src = ./.;
            installPhase = ''
              mkdir -p $out/share/plymouth/themes
              mv mikuboot $out/share/plymouth/themes/mikuboot
              sed -i "s@\/usr\/@$out\/@" $out/share/plymouth/themes/mikuboot/mikuboot.plymouth
            '';
          };
      });
}
